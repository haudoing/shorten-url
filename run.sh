#!/bin/sh

echo $DATABASE_URL

until psql $DATABASE_URL -c '\l'; do
    echo "Postgres is unavailable - sleeping" >&2
    sleep 1
done

echo "Postgres is up - continuing" >&1

cd shorten_url_svc
python manage.py migrate --noinput
daphne -b 0.0.0.0 -p 8000 shorten_url_svc.asgi:application
