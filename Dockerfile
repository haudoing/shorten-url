FROM python:3.8.3-alpine3.12

MAINTAINER Eric Lee <hau19860301@msn.com>
LABEL maintainer="Eric Lee <hau19860301@msn.com>"

WORKDIR /app

COPY ./requirements.txt /app/

RUN apk update && apk add --no-cache --virtual .build-deps \
    ca-certificates gcc postgresql-dev linux-headers musl-dev \
    python3-dev libffi-dev jpeg-dev zlib-dev \
    && apk add --update --no-cache libffi jpeg postgresql \
    && pip install -r requirements.txt \
    && apk del --no-cache .build-deps
