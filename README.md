shorten-url-service
===================

A shorten url service

### Installation
1. install docker on your machine
2. run command "docker-compose build && docker-compose up"
3. the local development server is up now!

### Note
1. .env file and all other environment variables store in this repo is for demo purpose. 
   Those sensitive information is better to handle by devop team during CI/CD workflow.
2. for persist database record you may want to configure the volume of postgres_db
3. The web page is only tested on Google Chrome

### feature request list
1. add a background service to check and perhaps remove website which is not reachable
2. target website page preview
3. for demo purpose, api does not protected by any mechanism. depends on requirement, 
   it may be a good idea to implement token authetication and rate limit etc
4. it's better to have swagger on dev for a better documentation
5. user activity tracking
6. insert record randomly instead of auto increment
