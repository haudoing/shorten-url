from rest_framework import serializers
from .utils import integer_to_path, path_to_integer


class ShortSlugField(serializers.IntegerField):
    '''convert between db value and json representation'''
    def to_representation(self, value):
        return integer_to_path(value)

    def to_internal_value(self, data):
        data = str(path_to_integer(data))
        if isinstance(data, str) and len(data) > self.MAX_STRING_LENGTH:
            self.fail('max_string_length')

        try:
            data = int(self.re_decimal.sub('', str(data)))
        except (ValueError, TypeError):
            self.fail('invalid')
        return data


class InputUrlSerializer(serializers.Serializer):
    url = serializers.URLField()


class ShortenUrlSerializer(serializers.Serializer):
    id = ShortSlugField()
    original_url = serializers.URLField()
