from django.db import models
from django.core.validators import URLValidator

# Create your models here.

class ShortenUrl(models.Model):
    # some url can be really long. this service should cover those url
    original_url = models.TextField(unique=True, validators=[URLValidator])
