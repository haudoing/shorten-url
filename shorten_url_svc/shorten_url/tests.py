import random
from django.test import TestCase
from shorten_url.utils import (
    MAX_NUMBER, new_base_to_base_10,
    base_10_to_new_base, integer_to_path,
    path_to_integer, SUPPORTED_DIGITS_LENGTH
)


class BaseConvertTest(TestCase):
    def setUp(self):
        pass

    def test_all_converters(self):
        random_valid_number = random.randint(1, MAX_NUMBER)
        new_base_number = base_10_to_new_base(random_valid_number)
        self.assertEqual(
            new_base_to_base_10(new_base_number),
            random_valid_number)
        self.assertEqual(
            len(integer_to_path(random_valid_number)),
            SUPPORTED_DIGITS_LENGTH)
        self.assertLessEqual(
            path_to_integer(new_base_number),
            MAX_NUMBER)
        self.assertGreaterEqual(
            path_to_integer(new_base_number),
            1)
