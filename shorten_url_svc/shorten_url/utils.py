import re
from django.conf import settings
# REF: https://stackoverflow.com/questions/742013/how-do-i-create-a-url-shortener

BASE = 62

SUPPORTED_DIGITS_LENGTH = settings.SUPPORTED_DIGITS_LENGTH

MAX_NUMBER = BASE ** SUPPORTED_DIGITS_LENGTH - 1

BASE_ORD = ['t', '8', '3', 'N', 'y', 'W', 'S', 'c', 'u', 'm', 'E', 'f',
            'x', 'X', 's', 'e', 'w', 'j', 'U', '4', 'a', '5', '2', 'b',
            'Q', 'O', 'g', 'H', 'T', 'Y', 'n', 'J', 'k', 'P', 'v', '1',
            'Z', 'I', 'C', 'R', 'L', '0', 'p', '6', '7', 'A', 'd', 'M',
            '9', 'i', 'o', 'G', 'K', 'l', 'z', 'D', 'h', 'r', 'F', 'q',
            'V', 'B']


class InvalidIntError(Exception):
    '''negative intger or exceed max number are invalid'''
    pass


class InvalidPathError(Exception):
    '''only support the length of SUPPORTED_DIGITS_LENGTH'''
    pass


def new_base_to_base_10(new_base_str):
    '''function to convert our custom new base to base 10'''
    integer = 0
    reverse_str = new_base_str[::-1]
    for inx, s in enumerate(reverse_str):
        integer += BASE ** inx * BASE_ORD.index(s)
    return integer


def base_10_to_new_base(integer):
    '''function to find our cutom new base number'''
    if integer == 0:
        return BASE_ORD[0]
    digits = []
    new_base_str = ''
    while integer > 0:
        remainder = integer % BASE
        digits.append(remainder)
        integer //=  BASE
    digits.reverse()
    for digit in digits:
        new_base_str += BASE_ORD[digit]
    return new_base_str


def integer_to_path(integer):
    '''function to convert integer to our custom path'''
    if integer > MAX_NUMBER or integer < 0:
        raise InvalidIntError()
    return base_10_to_new_base(integer).rjust(SUPPORTED_DIGITS_LENGTH, BASE_ORD[0])


def path_to_integer(path):
    '''return path to integer'''
    pattern = re.compile(fr'[a-zA-Z0-9]{{{SUPPORTED_DIGITS_LENGTH}}}')
    result = pattern.match(path)
    if not result:
        raise InvalidPathError()
    return new_base_to_base_10(path)
