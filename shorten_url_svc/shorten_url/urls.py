from django.urls import include, path
from .views import ShortenUrlViewSet
from rest_framework import routers


app_name = 'shorten_url'

router = routers.SimpleRouter()
router.register(r'shorten_url', ShortenUrlViewSet, 'shortenurl')
api_urlpatterns = ([
    path('', include((router.urls, app_name)))
], app_name)

urlpatterns = [

]