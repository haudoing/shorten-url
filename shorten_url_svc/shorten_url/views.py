import logging
from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import ShortenUrl
from .serializers import InputUrlSerializer, ShortenUrlSerializer
from .utils import integer_to_path, InvalidIntError


logger = logging.getLogger(__name__)


class ShortenUrlViewSet(viewsets.ViewSet):
    def create(self, request):
        input_serializer = InputUrlSerializer(data=request.data)
        if input_serializer.is_valid():
            url = input_serializer.validated_data.get('url')
            shorturl, created = ShortenUrl.objects.get_or_create(original_url=url)
            try:
                resp_serializer = ShortenUrlSerializer(shorturl)
            except InvalidIntError:
                return Response(
                    {'errors': (
                        'service exhausted.'
                        'do not accept any new urls from now on')},
                    status=status.HTTP_503_SERVICE_UNAVAILABLE)
            return Response(resp_serializer.data)
        else:
            return Response(
                {'errors': input_serializer.errors},
                status=status.HTTP_400_BAD_REQUEST)
