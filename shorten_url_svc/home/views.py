from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from shorten_url.utils import path_to_integer
from shorten_url.models import ShortenUrl

# Create your views here.


def redirect_view(request, short_slug):
    shorturl_id = path_to_integer(short_slug)
    shorturl = get_object_or_404(ShortenUrl, id=shorturl_id)
    return redirect(shorturl.original_url)


def index(request):
    return render(request, 'home/index.html', context={})
