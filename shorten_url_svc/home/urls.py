from django.urls import include, path, re_path
from django.conf import settings
from .views import redirect_view, index


app_name = 'home'

urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(
        fr'^(?P<short_slug>[a-zA-Z0-9]{{{settings.SUPPORTED_DIGITS_LENGTH}}})$',
        redirect_view, name='redirect'),
]
